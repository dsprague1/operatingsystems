import "io"
import "DSLib"

manifest
{
  nMaxWordLen = 256
}

//break command from user into
//individual words for text parser
//takes list as a parameter
let bOS_FillList(sList, sPrompt) be
{
  //variable to hold individual words in command
  let sWord = vec(nMaxWordLen);
  //length of current word
  let nTotalLen = 0;
  let nLen = 0;
  let tempLink;

  while true do
  {
    let c = byte nTotalLen of sPrompt;
    
    if c = 0 then break;
    test c = 32 then    
    {
       byte nLen of sWord := 0;
       nLen := 0;
       nTotalLen +:= 1;
       tempLink := new_link(sWord);
       add_link(sList, tempLink);
    }
    else
    {
        byte nLen of sWord := c;
        nLen +:= 1;
        nTotalLen +:= 1;
    }
  } 
}

let bOS_doParse(pList, sPrompt) be
{
  bOS_FillList(pList, sPrompt);
  out("finished splitting string\n");
//  switchon sPrompt into
//  {
//    case "":
      
//    endcase;
//  }
}

let start() be
{
  let heap = vec(1000);
  let pList;  
  //User input
  let sPrompt;
  init(heap, 100);
  pList := new_list();  
  assembly
  {
    type 'W'
    type 'e'
    type 'l'
    type 'c'
    type 'o'
    type 'm'
    type 'e'
    type '\n'
  }
  
  while(true) do
  {
    out("~> ");
    sPrompt := instr();
    bOS_doParse(pList, sPrompt);
  }
}